FROM python:3.8-alpine3.11 AS base

FROM base AS builder
COPY dummy_worker/ /build/dummy_worker/
# Poetry uses .gitignore to exclude files from the package
COPY pyproject.toml poetry.lock .gitignore /build/
# native dependencies are required
# poetry build complains if no git binary is installed
RUN set -ex; \
  apk add --no-cache --update \
    gcc \
    g++ \
    libffi-dev \
    musl-dev \
    openssl-dev ; \
  apk add --no-cache --update git; \
  pip install --no-cache-dir --upgrade setuptools; \
  pip install poetry; \
  poetry config virtualenvs.create false; \
  \
  cd build; \
  poetry build; \
  poetry export -f requirements.txt -o requirements.txt; \
  \
  pip install --root /install --no-warn-script-location -r /build/requirements.txt; \
  pip install --root /install --no-warn-script-location --no-dependencies /build/dist/*.whl

FROM base
COPY --from=builder /install /
CMD ["python", "-m", "dummy_worker"]
