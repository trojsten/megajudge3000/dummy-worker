# Dummy worker

A dummy worker that only acts as if it were testing submits, and occasionally
fails. It's primary purpose is to aid in development of the Controller until
the real Worker is ready, and to demonstrate how the M3K works.

## Getting started

  - Make sure you have `poetry` installed.
  - Run `poetry install` to install Python dependencies
  - Run `poetry run pre-commit install` to install pre-commit hooks.
